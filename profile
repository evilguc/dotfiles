if [[ -z $TMUX ]]; then
    if [ -e /usr/share/terminfo/x/xterm-256color ]; then
        export TERM='xterm-256color'
    else
        export TERM='xterm'
    fi
else
    if [ -e /usr/share/terminfo/s/screen-256color ]; then
        export TERM='screen-256color'
    else
        export TERM='screen'
    fi
fi

if [[ -d /usr/local/go ]]; then
  PATH="$PATH:/usr/local/go/bin"
fi
