### Base

```bash
ln -s $(pwd)/profile ~/.profile
ln -s $(pwd)/zshrc ~/.zshrc
ln -s $(pwd)/vimrc ~/.config/nvim/init.vim
ln -s $(pwd)/tmux.conf ~/.tmux.conf
ln -s $(pwd)/gpg-agent.conf ~/.gnupg/gpg-agent.conf
```

### Devbox base

```bash
$PACKAGES_INSTALL_CMD \
  curl \
  docker \
  docker-compose \
  git \
  keychain \
  mosh \
  neovim \
  pinentry-tty \
  ruby \
  tmux \
  zsh
```

### Git setup

```bash
git config --global user.name "John Doe"
git config --global user.email johndoe@example.com
git config --global core.editor nvim
git config --global merge.tool vimdiff
git config --global mergetool.vimdiff.path nvim
```

### Oh my zsh

```bash
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

### NeoVim

```bash
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
$PACKAGES_INSTALL_CMD \
  fzf
vim +PlugInstall +qall
```
