set nocompatible              " be iMproved
filetype off                  " required!

call plug#begin()

" original repos on GitHub
Plug 'nvim-tree/nvim-web-devicons'
Plug 'nvim-tree/nvim-tree.lua'
Plug 'sheerun/vim-polyglot'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-sleuth'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-endwise'
Plug 'tpope/vim-fugitive'
Plug 'mhinz/vim-signify'
Plug 'godlygeek/tabular'
Plug 'vim-scripts/matchit.zip'
Plug 'mattn/emmet-vim'
Plug 'bronson/vim-trailing-whitespace'
Plug 'majutsushi/tagbar'
Plug 'terryma/vim-multiple-cursors'
Plug 'chrisbra/Colorizer' " highlight colors
Plug 'mhinz/vim-startify'
Plug 'itchyny/lightline.vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" snipmate
" Plug 'MarcWeber/vim-addon-mw-utils'
" Plug 'tomtom/tlib_vim'
" Plug 'garbas/vim-snipmate'
" Plug 'honza/vim-snippets'

" Plug 'w0rp/ale'

" if has('nvim')
"   Plug 'ncm2/ncm2'
"   Plug 'autozimu/LanguageClient-neovim', {
"     \ 'branch': 'next',
"     \ 'do': 'bash install.sh',
"     \ }
"   Plug 'mhartington/nvim-typescript', { 'do': './install.sh' }
" endif

if has("termguicolors")
  set termguicolors
endif

" color shemes
Plug 'glepnir/oceanic-material' " oceanic_material
Plug 'rakr/vim-one'
Plug 'sainnhe/sonokai'

Plug 'othree/html5.vim'
Plug 'othree/yajs.vim'

" File search {{{
Plug 'dyng/ctrlsf.vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

nnoremap <C-P> :Files<CR>
nnoremap <C-[> :GFiles<CR>
" }}}


let g:polyglot_disabled = ['html', 'javascript']


call plug#end()

let mapleader = "," " set <leader> to comma

colorscheme sonokai
set background=dark

set cursorline " hilight the line cursor is currently on

set ruler " show column and line number in status line

set wildmenu " visual autocomplete for command menu

set showmatch " show matching braces

" disable mouse support
set mouse=

" Vim search {{{
set incsearch " search as type
set hlsearch " hilight search results
" turn of search hilight
nnoremap <leader><space> :nohlsearch<CR>
" }}}

" ale {{{
let g:ale_linters = {
\   'typescript': ['tslint'],
\}
" }}}

" Leader bindings {{{
" edit vimrc/zshrc and load vimrc bindings
nnoremap <Leader>ev :vsp $MYVIMRC<CR>
nnoremap <Leader>ez :vsp ~/.zshrc<CR>
nnoremap <Leader>sv :source $MYVIMRC<CR>

" add debugger call line
autocmd FileType typescript,javascript nnoremap <buffer> <Leader>d odebugger;<Esc>
autocmd FileType ruby nnoremap <buffer> <Leader>d orequire 'pry'; binding.pry<Esc>

" replace :id => 1 with id: 1
nnoremap <Leader>rs :%s/:\(['"]\)\(.\+\)\1\s\+=>/\1\2\1:/ge<CR>:%s/:\(\w\+\)\s\+=>/\1:/ge<CR>
" run rubocop autofix for current file
nnoremap <Leader>fr :!rubocop -a %<CR>

" CtrlSF.vim
nmap     <C-F>f <Plug>CtrlSFPrompt
vmap     <C-F>f <Plug>CtrlSFVwordPath
vmap     <C-F>F <Plug>CtrlSFVwordExec
nmap     <C-F>n <Plug>CtrlSFCwordPath
nmap     <C-F>p <Plug>CtrlSFPwordPath
nnoremap <C-F>t :CtrlSFToggle<CR>

" react bindings and abbreviations
abbreviate RPT PropTypes

" command abbreviations
cabbr <expr> %% expand('%:p:h')

if filereadable(glob("~/.vimrc_local"))
  source ~/.vimrc_local
endif

" File Tree
lua << EOF
-- examples for your init.lua

-- disable netrw at the very start of your init.lua (strongly advised)
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- set termguicolors to enable highlight groups
vim.opt.termguicolors = true

-- empty setup using defaults
require("nvim-tree").setup()

-- OR setup with some options
require("nvim-tree").setup({
  sort_by = "case_sensitive",
  view = {
    adaptive_size = false,
    mappings = {
      list = {
        { key = "u", action = "dir_up" },
      },
    },
  },
  renderer = {
    group_empty = true,
  },
  filters = {
    dotfiles = false,
  },
})
EOF

nnoremap <Leader>' :NvimTreeToggle<CR>
nnoremap <Leader>" :NvimTreeFindFile<CR>

" TagBar
nnoremap <Leader>] :Tagbar<CR>

" Fugitive
nnoremap <Leader>vd :Gdiff<CR>
nnoremap <Leader>vb :Gblame<CR>

" format xml
nnoremap <Leader>xml :silent %!xmllint --encode UTF-8 --format -<CR>
nnoremap <Leader>json :silent %!python3 -m json.tool<CR>
" }}}

" paste mode toggle
set pastetoggle=<F4>

" move lines around
nnoremap <C-j> :m .+1<CR>==
nnoremap <C-k> :m .-2<CR>==

set smartindent
set autoindent

" LanguageClient {{{
" Required for operations modifying multiple buffers like rename.
set hidden

let g:LanguageClient_serverCommands = {
    \ 'javascript': ['typescript-language-server', '--stdio'],
    \ 'javascript.jsx': ['typescript-language-server', '--stdio'],
    \ }
let g:LanguageClient_autoStart = 1

let g:deoplete#enable_at_startup = 1

nnoremap <silent> K :call LanguageClient_textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient_textDocument_definition()<CR>
nnoremap <silent> <Leader>r :call LanguageClient_textDocument_rename()<CR>
" }}}

" Modelines {{{
set modeline " make vim respect modelines
set modelines=1 " use only one modeline per file
" }}}

set number

set nowrap

" Folding {{{
nnoremap <space> za
" }}}


syntax on " enable syntax processing

filetype plugin indent on " load file type specific indent files

set colorcolumn=121

runtime macros/matchit.vim

set omnifunc=syntaxcomplete#Complete

" gitk
function GitkLogLine()
  let currentLine = line('.')
  execute '!gitk -L'.currentLine.','.currentLine.':'.expand('%@')
endfunction

" identify what highlight group is under cursor
map <F10> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
      \ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
      \ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

" yaml-path {{{
if !exists('g:yamlpath_sep')
  let g:yamlpath_sep = '/'
endif

if !exists('g:yamlpath_auto')
  let g:yamlpath_auto = 0
endif

function! Yamlpath(...)
  let sep = a:0 ? a:1 : g:yamlpath_sep
  let clean = systemlist('docker run --rm -i yaml-path --sep=' . sep . ' --line ' . line('.') . ' --col ' . string(col('.')-1), join(getline(1,'$') , "\n"))[0]
  redraw!
  echom clean
endfunction

command! -nargs=? Yamlpath call Yamlpath(<args>)

if has("autocmd") && g:yamlpath_auto
  au FileType yaml :autocmd CursorMoved * call Yamlpath()
endif

nmap <F9> :call Yamlpath()<CR>
" }}}

autocmd BufRead,BufNewFile *.yaml.template setlocal filetype=yaml.template

" vim:foldenable:foldmethod=marker:foldlevel=0
