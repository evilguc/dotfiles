# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

export UPDATE_ZSH_DAYS=14

ZSH_THEME="robbyrussell"
COMPLETION_WAITING_DOTS="true"

plugins=(git genpass)

[ -e ~/.oh-my-zshrc_local ] && source ~/.oh-my-zshrc_local

source $ZSH/oh-my-zsh.sh

source ~/.profile

PATH=$PATH:~/bin:./node_modules/.bin

export EDITOR=nvim
export SVN_EDITOR=nvim

alias ls='ls --color'

alias zshrc='$EDITOR ~/.zshrc'
alias vimrc='$EDITOR ~/.config/nvim/init.vim'

if which tmuxinator > /dev/null; then
    alias mux='tmuxinator'
fi

if which gem > /dev/null; then
    export PATH="$PATH:$(gem env | grep 'USER INSTALLATION' | cut -d':' -f2 | cut -d' ' -f2)/bin"
fi

alias vup='vagrant up'
alias vssh='vagrant ssh'
alias vhalt='vagrant halt'

### git aliases
alias gcan='git commit --amend --no-edit'
alias gcan!='git commit -a --amend --no-edit'
alias gch='git checkout'
alias gl='git log'
alias gpf='git push -f'
alias gpl='git pull'
alias gf='git fetch'
alias gs='git status'
alias gst='git stash'
alias gsta='git stash apply'
alias gdt='git difftool -y'
alias gdtc='git difftool -y --cached'
alias gbrms="git fetch -p && for branch in \`git branch -vv | grep ': gone]' | gawk '{print \$1}'\`; do git branch -D \$branch; done"

### docker
alias docker-dive='docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock wagoodman/dive:latest'
alias drrit='docker run --rm -it'

### docker-compose
alias dcb='docker-compose build'
alias dch='docker-compose stop'

if which nvim > /dev/null; then
    alias vim='nvim'
fi

alias ta='tmux a'

### jff
alias weather='curl wttr.in/'

alias alpine-here='docker run --rm -it -v $(pwd):/data -w /data alpine sh'
alias ubuntu-here='docker run --rm -it -v $(pwd):/data -w /data ubuntu bash'
alias nginx-here='docker run --rm -it -v $(pwd):/usr/share/nginx/html -p 8080:80 nginx'
alias node-here='docker run --rm -it -v $(pwd):/data -w /data -p 8080:80 --entrypoint=bash node'

alias muxit='cp ~/workplace/dotfiles/tmuxinator.yml ./.tmuxinator.yml'

eval $(keychain --eval --quiet --noask --agents ssh id_rsa)

disable r

[ -e ~/.zshrc_local ] && source ~/.zshrc_local

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
